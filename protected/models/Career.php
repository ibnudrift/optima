<?php

/**
 * This is the model class for table "tb_career".
 *
 * The followings are the available columns in table 'tb_career':
 * @property string $id
 * @property string $position
 * @property string $location
 * @property string $desc_en
 * @property string $desc_id
 * @property string $kualifikasi_en
 * @property string $kualifikasi_id
 */
class Career extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Career the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tb_career';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('position, location', 'length', 'max'=>225),
			array('desc_en, desc_id, kualifikasi_en, kualifikasi_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, position, location, desc_en, desc_id, kualifikasi_en, kualifikasi_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'position' => 'Position',
			'location' => 'Location',
			'desc_en' => 'Description English',
			'desc_id' => 'Description Indonesia',
			'kualifikasi_en' => 'Kualifikasi English',
			'kualifikasi_id' => 'Kualifikasi Indonesia',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('desc_en',$this->desc_en,true);
		$criteria->compare('desc_id',$this->desc_id,true);
		$criteria->compare('kualifikasi_en',$this->kualifikasi_en,true);
		$criteria->compare('kualifikasi_id',$this->kualifikasi_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}