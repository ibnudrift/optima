<?php
$this->breadcrumbs=array(
	'Product'=>array('index'),
	'Add',
);

$this->pageHeader=array(
	'icon'=>'fa fa-life-ring',
	'title'=>'Product',
	'subtitle'=>'Add Product',
);

$this->menu=array(
	// array('label'=>'List Product', 'icon'=>'th-list','url'=>array('index')),
);
?>

<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'input-product-form',
    // 'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
<div class="row-fluid">
	<div class="span12">
		<!-- ----------------- Action ----------------- -->
		<div class="widgetbox block-rightcontent">                        
		    <div class="headtitle">
		        <h4 class="widgettitle">Data File CSV</h4>
		    </div>
		    <div class="widgetcontent">

				<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>'Update Data',
					'htmlOptions'=>array('class'=>'btn-large', 'value'=>'submit', 'name'=>'submit'),
				)); ?>
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					// 'buttonType'=>'submit',
					// 'type'=>'info',
					'url'=>CHtml::normalizeUrl(array('index')),
					'label'=>'Cancel',
					'htmlOptions'=>array('class'=>'btn-large'),
				)); ?>

			<div id="or-order-grid" class="grid-view">
			   <div class="summary"></div>
			   <table class="items table table-bordered">
			      <thead>
			         <tr>
						<th>No Number</th>
						<th>Kategori</th>
						<th>Merk</th>
						<th>Image File Name</th>
						<th>Product Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Weight</th>
						<th>Status</th>
			         </tr>
			      </thead>
			      <tbody>
			      	<?php foreach ($dataCsv as $key => $value): ?>
			         <tr>
						<td><?php echo $value['0'] ?></td>
						<td><?php echo $value['6'] ?></td>
						<td><?php echo $value['2'] ?></td>
						<td><?php echo $value['0'].'.jpg' ?></td>
						<td><?php echo $value['1'] ?></td>
						<td><?php echo substr($value['5'], 0, 30) ?>....</td>
						<td><?php echo $value['7'] ?></td>
						<?php if (intval($value['9']) != 0): ?>
							<?php 
							$berat = substr((int) filter_var($value[9], FILTER_SANITIZE_NUMBER_INT), 0, -2);
							?>
							<td><?php echo $berat; ?></td>
						<?php else: ?>
							<td><?php echo '--'; ?></td>
						<?php endif ?>

						<td><?php echo '1'; ?></td>
			         </tr>
			      	<?php endforeach ?>
			      </tbody>
			   </table>
			</div>


				<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>'Update Data',
					'htmlOptions'=>array('class'=>'btn-large', 'value'=>'submit', 'name'=>'submit'),
				)); ?>
				<?php $this->widget('bootstrap.widgets.TbButton', array(
					// 'buttonType'=>'submit',
					// 'type'=>'info',
					'url'=>CHtml::normalizeUrl(array('index')),
					'label'=>'Cancel',
					'htmlOptions'=>array('class'=>'btn-large'),
				)); ?>
		    </div>
		</div>
	</div>
</div>

<?php $this->endWidget(); ?>

