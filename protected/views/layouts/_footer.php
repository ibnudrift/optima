<section class="outer_block_footer_top">
    <div class="prelatife container">
        <div class="inners text-center">
            <img src="<?php echo $this->assetBaseurl; ?>pic-mid_sticky-footer.png" alt="" class="d-block mx-auto img-fluid">
            <div class="py-2"></div>
            <h4>Dapatkan Bantuan & Informasi Melalui Whatsapp Chat Kami</h4>
            <div class="py-1"></div>
            <p>
                <a href="https://api.whatsapp.com/send?phone=<?php echo str_replace(' ', '', $this->setting['contact_wa']); ?>"><b><img src="<?php echo $this->assetBaseurl; ?>icons-wa.png" alt="" class="ic_wa">&nbsp; <?php echo str_replace('628', '08', $this->setting['contact_wa']) ?></b> (Click to chat)</a>
            </p>
            <div class="clear"></div>
        </div>
    </div>
</section>

<footer class="foot pt-4 pb-3">
    <div class="prelatife container">
        <div class="spaces_footn1"></div>
        <div class="row">
            <div class="col-md-20">
                <div class="sub_footer">
                    <a href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>"><span>PRODUK PART KAPAL</span></a>
                    <div class="py-2"></div>
                    <?php 
                    $criteria = new CDbCriteria;
                    $criteria->with = array('description');
                    $criteria->addCondition('t.parent_id = "0"');
                    $criteria->addCondition('t.type = :type');
                    $criteria->params[':type'] = 'category';
                    $criteria->order = 'sort ASC';
                    $CategoryAlls = PrdCategory::model()->findAll($criteria);
                    ?>
                    <ul class="list-unstyled">
                        <?php foreach ($CategoryAlls as $key => $value): ?>
                            <li><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=> $value->id, 'category-name'=>Slug::Create($value->description->name) )); ?>"><?php echo $value->description->name ?></a></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
            <div class="col-md-20">
                <div class="sub_footer">
                    <span>INFORMASI LAINNYA</span>
                    <div class="py-2"></div>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">Profil</a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Hubungi Kami</a></li>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">Blog</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-20">
                <div class="lgo_foot"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><img src="<?php echo $this->assetBaseurl; ?>logo-footer.jpg" alt="" class="img-fluid"></a></div>
                <div class="py-2"></div>
                <p>Copyright &copy; 2020 OPTIMA Parts <br>
                    Menjual aneka parts & peralatan kapal lainnya. <br>
                    All rights reserved.</p>
                <div class="clear"></div>
            </div>            
        </div>
    </div>
    <div class="py-2 my-1"></div>
    <div class="clear"></div>
    <div class="copyrights_ft">
        <div class="prelatife container">
            <p>Website design by <a title="Website Design Surabaya" target="_blank" href="https://www.markdesign.net/">Mark Design</a>.</p>
        </div>
    </div>
</footer>