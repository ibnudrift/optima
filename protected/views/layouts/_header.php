
<header class="head headers <?php if ($active_menu_pg == 'home/index'): ?>homes_head<?php endif ?> ">
  <div class="prelative container d-none d-sm-block hdn_top">
      <div class="row">
        <div class="col-md-30 col-sm-35">
          <div class="d-inline-block align-middle mr-4 pr-3 logo_heads"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><img src="<?php echo $this->assetBaseurl.'lgo-headers.png'; ?>" alt="" class="img img-fluid"></a></div>
          <div class="d-inline-block align-middle taglines_head"><span>Spare Part & Peralatan Kapal</span></div>
        </div>
        <div class="col-md-30 col-sm-25 text-right my-auto">
          <div class="d-inline-block align-middle">
            <ul class="list-inline m-0">
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">Profil</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Hubungi Kami</a></li>
              <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">Blog</a></li>
            </ul>
          </div>
          <div class="d-inline-block align-middle mx-3 block_separategrey"></div>
          <div class="d-inline-block align-middle wa_head">
            <span><a href="https://api.whatsapp.com/send?phone=<?php echo str_replace(' ', '', $this->setting['contact_wa']); ?>"><b>WHATSAPP</b> &nbsp;&nbsp;<i class="fa fa-whatsapp"></i>&nbsp;&nbsp; <?php echo str_replace('628', '08', $this->setting['contact_wa']) ?></a></span>
          </div>
        </div>
      </div>
      <div class="clear"></div>
  </div>
  
  <?php 
  $criteria = new CDbCriteria;
  $criteria->with = array('description');
  $criteria->addCondition('t.parent_id = "0"');
  $criteria->addCondition('t.type = :type');
  $criteria->params[':type'] = 'category';
  $criteria->order = 'sort ASC';
  $CategoryAlls = PrdCategory::model()->findAll($criteria);
  ?>
  <div class="bottoms_head border_top d-none d-sm-block">
    <div class="prelatife container">
      <div class="tops_menu justify-content-center d-block mx-auto">
        <ul class="list-inline justify-content-center d-block mx-auto">
          <?php foreach ($CategoryAlls as $key => $value): ?>
          <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=> $value->id, 'category-name'=>Slug::Create($value->description->name) )); ?>"><?php echo strtoupper($value->description->name); ?></a></li>
          <?php endforeach ?>
          <li class="list-inline-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'type'=> 'harga_turun')); ?>">HARGA TURUN</a></li>
        </ul>
      </div>
      <div class="clear"></div>
    </div>
  </div>

  <!-- Head mobile -->
  <div class="views_headmobile d-block d-sm-none">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><img src="<?php echo $this->assetBaseurl.'lgo-headers.png'; ?>" alt="" class="img img-fluid"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">Profil</a></li>
          <?php foreach ($CategoryAlls as $key => $value): ?>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=> $value->id, 'category-name'=>Slug::Create($value->description->name) )); ?>"><?php echo ucwords(strtolower($value->description->name)); ?></a></li>
          <?php endforeach ?>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/index', 'type'=> 'harga_turun')); ?>">Harga Turun</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">Blog</a></li>
          <li class="nav-item"><a class="nav-link" href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Hubungi Kami</a></li>
        </ul>
      </div>
    </nav>
  </div>
  <!-- End Head mobile -->
</header>


<?php
/*
<div class="outer-blok-black-menuresponss-hides d-none">
  <div class="prelatife container">
    <div class="clear height-45"></div>
    <div class="fright">
      <div class="hidesmenu-frightd"><a href="#" class="closemrespobtn"><img src="<?php echo $this->assetBaseurl ?>closen-btn.png" alt=""></a></div>
    </div>
    <div class="py-3"></div>
    <div class="blocksn_logo-centers d-block mx-auto text-center">
      <img src="<?php echo $this->assetBaseurl ?>logo-nippo.svg" style="max-width: 167px;" alt="logo" class="img img-fluid mx-auto">
    </div>
    <div class="py-4"></div>
    <div class="menu-sheader-datals">
      <ul class="list-unstyled">
        <?php foreach ($mod_kategh as $key => $value): ?>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/dining', 'id'=>$value->id, 'name'=> strtolower($value->title))); ?>"><?php echo ucwords(strtolower($value->title)); ?></a></li>       
        <?php endforeach ?>
      </ul>
      <div class="py-2"></div>
      <div class="py-1"></div>
      <div class="line-separate d-block mx-auto"></div>
      <div class="py-2"></div>
      <ul class="list-unstyled n_menu2">
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About Us</a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/quality')); ?>">Our Quality</a></li>
        <li><a href="#">Nippo Profile Book</a></li>
        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact Us</a></li>
      </ul>
      <div class="py-2"></div>
      <div class="line-separate d-block mx-auto"></div>
      <div class="py-1"></div>
      <div class="py-2"></div>
    </div>
    <div class="blocks-info-menubtm text-center">
      <span>Social Media</span>
      <div class="py-1"></div>
      <ul class="list-inline">
        <li class="list-inline-item"><a target="_blank" href="<?php echo $this->setting['url_instagram'] ?>">Instagram</a></li>
        <li class="list-inline-item">|</li>
        <li class="list-inline-item"><a target="_blank" href="<?php echo $this->setting['url_youtube'] ?>">Youtube</a></li>
      </ul>
      <div class="clear"></div>
    </div>

    <div class="clear"></div>
  </div>
  <div class="clear"></div>
</div>

<script type="text/javascript">
  $(function(){
    // show and hide menu responsive
    $('a.showmenu_barresponsive').on('click', function() {
      $('.outer-blok-black-menuresponss-hides').slideToggle('slow');
      return false;
    });
    $('a.closemrespobtn').on('click', function() {
      $('.outer-blok-black-menuresponss-hides').slideUp('slow');
      return false;
    });

  })
</script>
*/ 
?>