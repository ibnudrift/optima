<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<?php echo $this->renderPartial('//layouts/_header', array()); ?>

<?php 
// get slides
// $criteria = new CDbCriteria;
// $criteria->with = array('description');
// $criteria->addCondition('active = "1"');
// $criteria->addCondition('description.language_id = :language_id');
// $criteria->params[':language_id'] = $this->languageID;
// $criteria->order = 't.urutan ASC';
// $slides = Slide::model()->findAll($criteria);
?>
<div class="spacer_head_mob d-block d-sm-none"></div>

<?php echo $content ?>

<script type="text/javascript">
    $(document).ready(function(){
        
    });
</script>

<?php echo $this->renderPartial('//layouts/_footer', array()); ?>

<?php $this->endContent(); ?>
