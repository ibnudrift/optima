
<section class="cover-insides prelatife">
  <div class="prelatife container">
    <div class="inners">
      <div class="row">
        <div class="col-md-15 my-auto order-sm-1 order-2">
          <div class="boxed-text">
            <h1 class="mb-0">Blog</h1>
            <h6 class="mb-0">OPTIMA MARINE PARTS SHOP</h6>
            <p><i>Spare Part & Peralatan Kapal</i></p>
          </div>
        </div>
        <div class="col-md-45 order-sm-2 order-1">
          <div class="pictures">
            <img src="<?php echo $this->assetBaseurl. 'ill-blog.jpg'; ?>" alt="" class="img img-fluid">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="about_outer_content back-white">
  <div class="prelatife container">
    <div class="py-4"></div>
    <div class="py-3 d-none d-sm-block"></div>
    
    <div class="content-text text-center insubpage">
      
      <div class="boxed-details_blog text-center">
        <h2><?php echo $dataBlog->description->title; ?></h2>  
        <span class="dates"><?php echo date("d F Y", strtotime($dataBlog->date_input)); ?></span>
        <div class="py-3"></div>
        <div class="pictures"><img src="<?php echo Yii::app()->baseUrl.'/images/blog/'. $dataBlog->image; ?>" alt="<?php echo $dataBlog->description->title ?>" class="img img-fluid"></div>
        <div class="py-3"></div>
        <?php echo $dataBlog->description->content; ?>
        <div class="clear"></div>
      </div>

      <div class="py-4 my-3"></div>
      
      <div class="bx_ntops_red">
        <div class="row">
          <div class="col">
            <h6>BLOG OPTIMA MARINE PARTS SHOP lainnya</h6>
          </div>
          <div class="col text-right">
            <a href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>" class="m-0">KEMBALI KE INDEX BLOG</a>
          </div>
        </div>
      </div>
      <!-- start list blog -->
      <?php if ($dataBlogs): ?>
      <div class="lists_blog_data pg_blog">
        <div class="row">
          <?php foreach ($dataBlogs as $key => $value): ?>
          <div class="col-md-15 col-30">
            <div class="items mb-4">
              <div class="pict">
                <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id' => $value->id )); ?>"><img class="img img-fluid w-100"src="<?php echo $this->assetBaseurl.'../../images/blog/'; ?><?php echo $value->image ?>" alt="<?php echo ucwords($value->description->title); ?>"></a>
              </div>
              <div class="info py-3">
                <h5 class="mb-0"><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id' => $value->id )); ?>"><?php echo ucwords($value->description->title); ?></a></h5>
              </div>
            </div>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
      <?php endif ?>
      <!-- end list blog -->

      <div class="clear"></div>
    </div>

    <div class="py-4"></div>
  </div>
</section>

<script type="text/javascript">
    $(function(){
        $('.outer_block_footer_top').addClass('backs-white');
    })
</script>




<?php
/*
<section class="product-sec-1">
  <div class="prelative container">
    <div class="row">
      <div class="col-md-15">
      <div class="box-konten-kiri">
          <h5>Blog</h5>        
      </div>
      </div>
      <div class="col-md-45 rights_cont_def">
        <div class="clear clearfix"></div>

        <?php if ($dataBlog): ?>
        <div class="row default-data">
            <?php foreach ($dataBlog->getData() as $key => $value): ?>
                <div class="col-md-20">
                    <div class="box-content">
                        <div class="image">
                            <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id' => $value->id )); ?>"><img class="img img-fluid w-100"src="<?php echo $this->assetBaseurl.'../../images/blog/'; ?><?php echo $value->image ?>" alt=""></a>
                        </div>
                        <div class="title">
                            <p><?php echo ucwords($value->description->title); ?></p>
                        </div>
                        <div class="klik">
                        <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id' => $value->id )); ?>"><p>Read More</p></a>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
        <?php endif ?>
        <div class="py-4"></div>
        <div class="textaboveheader-landing page">
              <?php 
               $this->widget('CLinkPager', array(
                  'pages' => $dataBlog->getPagination(),
                  'header'=>'',
                  'footer'=>'',
                  'lastPageCssClass' => 'd-none',
                  'firstPageCssClass' => 'd-none',
                  'nextPageCssClass' => 'd-none',
                  'previousPageCssClass' => 'd-none',
                  'itemCount'=> $dataBlog->totalItemCount,
                  'htmlOptions'=>array('class'=>'pagination'),
                  'selectedPageCssClass'=>'active',
              ));
           ?>
          </div>

      </div>
    </div>
  </div>
</section>
*/ ?>