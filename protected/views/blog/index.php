
<section class="cover-insides prelatife">
  <div class="prelatife container">
    <div class="inners">
      <div class="row">
        <div class="col-md-15 my-auto order-sm-1 order-2">
          <div class="boxed-text">
            <h1 class="mb-0">Blog</h1>
            <h6 class="mb-0">OPTIMA MARINE PARTS SHOP</h6>
            <p><i>Spare Part & Peralatan Kapal</i></p>
          </div>
        </div>
        <div class="col-md-45 order-sm-2 order-1">
          <div class="pictures">
            <img src="<?php echo $this->assetBaseurl. 'ill-blog.jpg'; ?>" alt="" class="img img-fluid">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="about_outer_content back-white">
  <div class="prelatife container">
    <div class="py-4"></div>
    <div class="py-3 d-none d-sm-block"></div>
    
    <div class="content-text text-center insubpage">
      <h2 class="title-page">BLOG OPTIMA MARINE PARTS SHOP</h2>      
      <div class="py-4"></div>
      
      <!-- start list blog -->
      <div class="lists_blog_data pg_blog">
        <?php if (count($dataBlog->getData()) > 0): ?>
        <div class="row justify-content-center text-center">
          <?php foreach ($dataBlog->getData() as $key => $value): ?>
          <div class="col-md-15 col-30">
            <div class="items mb-4">
              <div class="pict">
                <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id' => $value->id )); ?>"><img class="img img-fluid w-100"src="<?php echo $this->assetBaseurl.'../../images/blog/'; ?><?php echo $value->image ?>" alt="<?php echo ucwords($value->description->title); ?>"></a>
              </div>
              <div class="info py-3">
                <h5 class="mb-0"><a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id' => $value->id )); ?>"><?php echo ucwords($value->description->title); ?></a></h5>
              </div>
            </div>
          </div>
          <?php endforeach; ?>
        </div>

      <?php else: ?>
        <h6>Sory, Data is empty!</h6>
      <?php endif; ?>
      </div>

      <div class="py-3"></div>
      <div class="text-center blocks_pagination">
        <nav aria-label="Page navigation example">
          <?php 
               $this->widget('CLinkPager', array(
                  'pages' => $dataBlog->getPagination(),
                  'header'=>'',
                  'footer'=>'',
                  'lastPageCssClass' => 'd-none',
                  'firstPageCssClass' => 'd-none',
                  'nextPageCssClass' => 'd-none',
                  'previousPageCssClass' => 'd-none',
                  'itemCount'=> $dataBlog->totalItemCount,
                  'htmlOptions'=>array('class'=>'pagination justify-content-center text-center'),
                  'selectedPageCssClass'=>'active',
              ));
           ?>
        </nav>
      </div>
      <!-- end list blog -->

      <div class="clear"></div>
    </div>

    <div class="py-4"></div>
  </div>
</section>

<script type="text/javascript">
    $(function(){

        $('.outer_block_footer_top').addClass('backs-white');

        $('.blocks_pagination ul.pagination li').addClass('page-item');
        $('.blocks_pagination ul.pagination li a').addClass('page-link');

    });
</script>




<?php
/*
<section class="product-sec-1">
  <div class="prelative container">
    <div class="row">
      <div class="col-md-15">
      <div class="box-konten-kiri">
          <h5>Blog</h5>        
      </div>
      </div>
      <div class="col-md-45 rights_cont_def">
        <div class="clear clearfix"></div>

        <?php if ($dataBlog): ?>
        <div class="row default-data">
            <?php foreach ($dataBlog->getData() as $key => $value): ?>
                <div class="col-md-20">
                    <div class="box-content">
                        <div class="image">
                            <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id' => $value->id )); ?>"><img class="img img-fluid w-100"src="<?php echo $this->assetBaseurl.'../../images/blog/'; ?><?php echo $value->image ?>" alt=""></a>
                        </div>
                        <div class="title">
                            <p><?php echo ucwords($value->description->title); ?></p>
                        </div>
                        <div class="klik">
                        <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id' => $value->id )); ?>"><p>Read More</p></a>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
        <?php endif ?>
        <div class="py-4"></div>
        <div class="textaboveheader-landing page">
              <?php 
               $this->widget('CLinkPager', array(
                  'pages' => $dataBlog->getPagination(),
                  'header'=>'',
                  'footer'=>'',
                  'lastPageCssClass' => 'd-none',
                  'firstPageCssClass' => 'd-none',
                  'nextPageCssClass' => 'd-none',
                  'previousPageCssClass' => 'd-none',
                  'itemCount'=> $dataBlog->totalItemCount,
                  'htmlOptions'=>array('class'=>'pagination'),
                  'selectedPageCssClass'=>'active',
              ));
           ?>
          </div>

      </div>
    </div>
  </div>
</section>
*/ ?>