<?php 
// query category
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = "0"');
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->order = 'sort ASC';
$CategoryAlls = PrdCategory::model()->findAll($criteria);
?>

<section class="outers_wrapper">
    
    <section class="home_blue_content1 pg_insides">
        <div class="prelatife container">

        <div class="block_tops_banner_filters prelatife">
            <div class="py-2"></div>
            <div class="row py-4">
                <div class="col-md-3"></div>
                <div class="col-md-54">
                    <div class="filter_pninside_products">
                        <h5>Cari spare part & aneka peralatan kapal yang anda inginkan</h5>
                        <div class="py-2"></div>
                        <div class="clear"></div>
                        <!-- start form -->
                        <form class="form-inline" method="get" action="<?php echo CHtml::normalizeUrl(array('/product/index')); ?>">
                          <input type="text" name="q" class="form-control mb-2 mr-sm-2" placeholder="Ketik pencarian anda">
                          <select name="category" id="" class="form-control mb-2 mr-sm-2">
                              <option value="">Pilih Category</option>
                              <?php foreach ($CategoryAlls as $key => $value): ?>
                              <option value="<?php echo $value->id ?>"><?php echo $value->description->name ?></option>
                              <?php endforeach ?>
                          </select>

                          <button type="submit" class="btn btn-primary mb-2">Cari</button>
                        </form>
                        <!-- end form                   -->
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>

        <div class="clear py-1"></div>

        <div class="backs_block_widget-category px-5 pt-4">
            <div class="tops pb-2">
                <div class="breadcrumbs">
                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active" aria-current="page">Kategori PRODUK</li>
                      </ol>
                    </nav>
                </div>
                <div class="py-2"></div>
                <div class="clear"></div>
            </div>
            
            <?php 
            // query category
            $criteria = new CDbCriteria;
            $criteria->with = array('description');
            $criteria->addCondition('t.parent_id = "0"');
            $criteria->addCondition('t.type = :type');
            $criteria->params[':type'] = 'category';
            $criteria->order = 'sort ASC';
            $CategoryAlls = PrdCategory::model()->findAll($criteria);
            ?>
            <div class="lists_item_categorys">
                <div class="row">
                    <?php foreach ($CategoryAlls as $key => $value): ?>
                    <div class="col-md-15 col-30">
                        <div class="items">
                            <div class="pict prelatife">
                                <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=> $value->id, 'category-name'=>Slug::Create($value->description->name))); ?>">
                                    <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(260,260, '/images/category/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="<?php echo $value->description->name; ?>" class="img img-fluid">
                                    <span class="nods_cream"><i class="fa fa-chevron-right"></i></span>
                                </a>
                            </div>
                            <div class="info">
                                <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'category'=> $value->id, 'category-name'=>Slug::Create($value->description->name))); ?>"><h4><?php echo $value->description->name ?></h4></a>
                                <div class="py-1"></div>
                                <p><?php echo $value->description->content ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="py-3"></div>
        <div class="clear clearfix"></div>
        </div>
    </section>

    <div class="clear"></div>
    
    <!-- product terbaru -->
    <section class="back-whide-def py-5 middles_home_conts3">
        <div class="prelatife container my-5">
            <div class="inners">

                <div class="wiget-tops-title">
                    <div class="row">
                        <div class="col">
                            <h4>PRODUK TERBARU</h4>
                        </div>
                        <div class="col text-right">
                            <div class="backs_andviewall">
                                <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'type'=>'produk_terbaru')); ?>" class="btn btn-link">Lihat Semua Produk &nbsp;<i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="py-3"></div>
                
                <!-- start list product -->
                <?php 

                    $criteria = new CDbCriteria;
                    $criteria->with = array('description');
                    $criteria->addCondition('t.status = "1"');
                    $criteria->order = 't.id DESC';
                    $criteria->limit = 4;
                    $products = PrdProduct::model()->findAll($criteria);
                ?>


                <div class="lists_products_defitems">
                    <div class="row">

                        <?php foreach ($products as $key => $value): ?>
                        <div class="col-md-15 col-30">
                            <div class="items mb-3">
                                <div class="pict">
                                    <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $value->id, 'product-name'=>Slug::Create($value->description->name) )); ?>">
                                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(260,260, '/images/product/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="<?php echo $value->description->name ?>" class="img-fluid">
                                    </a>
                                </div>
                                <div class="infos py-2">
                                    <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $value->id, 'product-name'=>Slug::Create($value->description->name) )); ?>"><h5><?php echo $value->description->name ?></h5></a>
                                    <p>
                                        <b>Rp <?php echo ($value->harga) ?></b> 
                                        <?php if ($value->harga_coret != 0 && $value->harga_coret !== null): ?>
                                        &nbsp;<small>Rp 15,000,000,-</small>
                                        <?php endif ?>
                                    </p>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <?php endforeach ?>

                    </div>
                </div>
                <!-- end list product -->
                
                <div class="clear"></div>
            </div>
        </div>
    </section>

    <section class="back-whide-def2 py-5 middles_home_conts3">
        <div class="prelatife container my-5">
            <div class="inners">

                <div class="wiget-tops-title">
                    <div class="row">
                        <div class="col">
                            <h4>PRODUK PALING BANYAK DILIHAT</h4>
                        </div>
                        <div class="col text-right">
                            <div class="backs_andviewall">
                                <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'type'=>'banyak_dilihat')); ?>" class="btn btn-link">Lihat Semua Produk &nbsp;<i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="py-3"></div>
                
                <!-- start list product -->
                <?php 

                    $criteria = new CDbCriteria;
                    $criteria->with = array('description');
                    $criteria->addCondition('t.status = "1"');
                    $criteria->order = 't.views DESC';
                    $criteria->limit = 4;
                    $products = PrdProduct::model()->findAll($criteria);
                ?>
                
                <div class="lists_products_defitems">
                    <div class="row">

                        <?php foreach ($products as $key => $value): ?>
                        <div class="col-md-15 col-30">
                            <div class="items mb-3">
                                <div class="pict">
                                    <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $value->id, 'product-name'=>Slug::Create($value->description->name) )); ?>">
                                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(260,260, '/images/product/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="<?php echo $value->description->name ?>" class="img-fluid">
                                    </a>
                                </div>
                                <div class="infos py-2">
                                    <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $value->id, 'product-name'=>Slug::Create($value->description->name) )); ?>"><h5><?php echo $value->description->name ?></h5></a>
                                    <p>
                                        <b>Rp <?php echo ($value->harga) ?></b> 
                                        <?php if ($value->harga_coret != 0 && $value->harga_coret !== null): ?>
                                        &nbsp;<small>Rp 15,000,000,-</small>
                                        <?php endif ?>
                                    </p>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <?php endforeach ?>

                    </div>
                </div>
                <!-- end list product -->
                
                <div class="clear"></div>
            </div>
        </div>
    </section>

    <section class="back-orange-def py-5 middles_home_conts4">
        <div class="prelatife container my-2">
            <div class="inners">

                <div class="wiget-tops-title">
                    <div class="row">
                        <div class="col">
                            <h4 class="m-0">PRODUK HARGA TURUN</h4>
                        </div>
                        <div class="col text-right">
                            <div class="backs_andviewall">
                                <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'type'=> 'harga_turun')); ?>" class="btn btn-link">Lihat Semua Produk &nbsp;<i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="py-3"></div>

                <!-- Start list product -->
                <?php 

                    $criteria = new CDbCriteria;
                    $criteria->with = array('description');
                    $criteria->addCondition('t.status = "1"');
                    $criteria->addCondition('t.terlaris = "1"');
                    $criteria->order = 't.id DESC';
                    $criteria->limit = 4;
                    $products = PrdProduct::model()->findAll($criteria);
                ?>
                <div class="lists_products_defitems prds_bottom">
                    <div class="row">
                        <?php foreach ($products as $key => $value): ?>
                        <div class="col-md-15 col-30">
                            <div class="items mb-3">
                                <div class="pict">
                                    <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $value->id, 'product-name'=>Slug::Create($value->description->name) )); ?>">
                                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(260,260, '/images/product/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="<?php echo $value->description->name ?>" class="img-fluid">
                                    </a>
                                </div>
                                <div class="infos py-2">
                                    <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $value->id, 'product-name'=>Slug::Create($value->description->name) )); ?>"><h5><?php echo $value->description->name ?></h5></a>
                                    <p>
                                        <b>Rp <?php echo ($value->harga) ?></b> 
                                        <?php if ($value->harga_coret != 0 && $value->harga_coret !== null): ?>
                                        &nbsp;<small>Rp 15,000,000,-</small>
                                        <?php endif ?>
                                    </p>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <!-- End list product -->

                <div class="py-2"></div>

                <div class="d-block mx-auto blocks_nall">
                    <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'type'=> 'harga_turun')); ?>" class="btn btn-link btns_viewn_all">Lihat Semua Produk Harga Turun</a>
                    <div class="clear"></div>
                </div>
                
                <div class="clear"></div>
            </div>
        </div>
    </section>

    <div class="clear"></div>
</section>