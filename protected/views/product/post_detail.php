<?php 
// query category
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = "0"');
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->order = 'sort ASC';
$CategoryAlls = PrdCategory::model()->findAll($criteria);
?>

<section class="outers_wrapper">
    
    <section class="home_blue_content1 pg_insides">
        <div class="prelatife container">

        <div class="block_tops_banner_filters prelatife">
            <div class="py-2"></div>
            <div class="row py-4">
                <div class="col-md-3"></div>
                <div class="col-md-54">
                    <div class="filter_pninside_products">
                        <h5>Cari spare part & aneka peralatan kapal yang anda inginkan</h5>
                        <div class="py-2"></div>
                        <div class="clear"></div>
                        <!-- start form -->
                        <form class="form-inline" method="get" action="<?php echo CHtml::normalizeUrl(array('/product/index')); ?>">
                          <input type="text" name="q" class="form-control mb-2 mr-sm-2" placeholder="Ketik pencarian anda">
                          <select name="category" id="" class="form-control mb-2 mr-sm-2">
                              <option value="">Pilih Category</option>
                              <?php foreach ($CategoryAlls as $key => $value): ?>
                              <option value="<?php echo $value->id ?>"><?php echo $value->description->name ?></option>
                              <?php endforeach ?>
                          </select>

                          <button type="submit" class="btn btn-primary mb-2">Cari</button>
                        </form>
                        <!-- end form                   -->
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>

        <div class="clear py-1"></div>

        <div class="backs_block_widget-category px-5 pt-4">
            <div class="tops pb-2">
                <div class="breadcrumbs">
                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>">Kategori PRODUK</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo $strCategory->description->name; ?></li>
                      </ol>
                    </nav>
                </div>
                <div class="py-2"></div>
                <div class="clear"></div>
            </div>
            
            <div class="tops_prdn_pagelist pt-2 pb-5">
                <div class="row">
                    <div class="col-md-45">
                        <h4 class="name-product"><?php echo ucwords($data->description->name); ?></h4>
                    </div>
                    <div class="col-md-15">
                        <a href="#" class="btn btn-link p-0 backs_def_p">Kembali Ke Kategori Produk &nbsp;&nbsp;<i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
                
                <!-- start box detail product -->
                <div class="row boxed_product_details">
                    <div class="col-md-30">
                        <div class="pictures_bg"><img src="<?php echo $this->assetBaseurl.'../../images/product/'. $data->image ?>" alt="" class="img img-fluid"></div>
                        <div class="py-2"></div>
                        <div class="lisn_thumb_pict">
                            <ul class="list-inline m-0 mr-0">
                                <li class="list-inline-item">
                                    <a data-src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(124,124, '/images/product/'. $data->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" href="#"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(124,124, '/images/product/'. $data->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="" class="img img-fluid"></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#"><img src="<?php echo $this->assetBaseurl.'other-picts-small.jpg' ?>" alt="" class="img img-fluid"></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#"><img src="<?php echo $this->assetBaseurl.'other-picts-small.jpg' ?>" alt="" class="img img-fluid"></a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#"><img src="<?php echo $this->assetBaseurl.'other-picts-small.jpg' ?>" alt="" class="img img-fluid"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-30">
                        <div class="descriptions infos">
                            <table class="table">
                                <tr>
                                    <td width="28%"><b>SKU/ID NUMBER</b></td>
                                    <td><p><?php echo $data->kode; ?></p></td>
                                </tr>
                                <tr>
                                    <td><b>DIMENSI</b></td>
                                    <td><p><?php echo $data->dimensi ?></p></td>
                                </tr>
                                <tr>
                                    <td><b>BERAT</b></td>
                                    <td><p><?php echo ($data->berat / 1000). ' Kg' ?></p></td>
                                </tr>
                                <?php if ($data->description->desc): ?>
                                <tr>
                                    <td><b>DESKRIPSI</b></td>
                                    <td>
                                        <?php echo $data->description->desc; ?>
                                    </td>
                                </tr>
                                <?php endif ?>
                                <?php 
                                $string = 'Saya ingin Order, '. "\r\n" . $data->description->name. ' dari Optima - Marine Parts Shop';
                                ?>
                                <tr>
                                    <td colspan="2">
                                        <a href="https://api.whatsapp.com/send?phone=6281331330008&text=<?php echo rawurlencode($string); ?>" class="btn btn-success btns_def_order b_green">Inkuiri Via Whatsapp</a>
                                        <!-- &nbsp;&nbsp;
                                        <button class="btn btn-success btns_def_order b_orange">Inkuiri Form Online</button> -->
                                    </td>
                                </tr>
                            </table>
                            <div class="py-3"></div>
                            <div class="small_rights_info">
                                <p><b>MENGAPA MEMBELI DI OPTIMA PARTS - TOKO ONLINE JUAL SPARE PARTS & ANEKA PERALATAN KAPAL</b></p>
                                <p>OPTIMA Parts, anak perusahaan subsidiari dari PT. Optima Prima Metal Sinergi, Tbk (OPMS) melayani jual beli aneka spare parts dan aneka peralatan kapal berkualitas. Anda dapat menemukan beraneka produk layak pakai dan bermutu dengan harga sangat rendah dan bersaing. OPTIMA Parts menjual spare parts dan aneka peralatan kapal bekas berkualitas seperti kabel listrik kapal, kabel gauge kapal, instrumen ruang kemudi kapal, baling-baling propeller kapal, jangkar kapal, genset / generator listrik kapal, pompa air laut kapal, stabiliser elektronik kapal, dan berbagai peralatan kelistrikan kapal lainnya. Anda dapt menghubungi OPTIMA Parts untuk lebih spesifik menemukan produk yang ingin anda cari untuk kapal anda.</p>
                                <div class="clear"></div>
                            </div>
                            <div class="clear clearfix"></div>
                        </div>
                    </div>
                </div>
                <!-- end box detail product -->

            <div class="clearfix"></div>
        </div>
        <div class="py-5"></div>
        <div class="clear clearfix"></div>
        </div>
    </section>

    <div class="clear"></div>

    <section class="back-whide-def2 py-5 middles_home_conts3 blocks_other_prd">
        <div class="prelatife container mt-5 mb-3">
            <div class="inners">

                <div class="wiget-tops-title">
                    <div class="row">
                        <div class="col">
                            <h4>REKOMENDASI PRODUK SPARE PART KAPAL LAINNYA</h4>
                        </div>
                    </div>
                </div>
                <div class="py-3"></div>
                
                <!-- start list product -->
                <div class="lists_products_defitems">
                    <div class="row">

                        <?php foreach ($other as $key => $value): ?>
                        <div class="col-md-15 col-30">
                            <div class="items mb-3">
                                <div class="pict">
                                    <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $value->id, 'product-name'=>Slug::Create($value->description->name) )); ?>">
                                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(260,260, '/images/product/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="<?php echo $value->description->name ?>" class="img-fluid">
                                    </a>
                                </div>
                                <div class="infos py-2">
                                    <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $value->id, 'product-name'=>Slug::Create($value->description->name) )); ?>"><h5><?php echo $value->description->name ?></h5></a>
                                    <p>
                                        <b>Rp <?php echo ($value->harga) ?></b> 
                                        <?php if ($value->harga_coret != 0 && $value->harga_coret !== null): ?>
                                        &nbsp;<small>Rp 15,000,000,-</small>
                                        <?php endif ?>
                                    </p>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <?php endforeach ?>

                    </div>
                </div>
                <!-- end list product -->

                <div class="py-2"></div>
                <div class="wiget-tops-title">
                    <div class="row">
                        <div class="col">
                            &nbsp;
                        </div>
                        <div class="col text-right">
                            <div class="backs_andviewall">
                                <a href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>" class="btn btn-link">Lihat Semua Produk &nbsp;<i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="clear"></div>
            </div>
        </div>
    </section>

    <div class="clear"></div>
</section>

<script type="text/javascript">
    $(function(){
        $('.outer_block_footer_top').addClass('backs-white2');
    })
</script>