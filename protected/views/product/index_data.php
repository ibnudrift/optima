<?php 
// query category
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('t.parent_id = "0"');
$criteria->addCondition('t.type = :type');
$criteria->params[':type'] = 'category';
$criteria->order = 'sort ASC';
$CategoryAlls = PrdCategory::model()->findAll($criteria);
?>

<section class="outers_wrapper">
    
    <section class="home_blue_content1 pg_insides">
        <div class="prelatife container">

        <div class="block_tops_banner_filters prelatife">
            <div class="py-2"></div>
            <div class="row py-4">
                <div class="col-md-3"></div>
                <div class="col-md-54">
                    <div class="filter_pninside_products">
                        <h5>Cari spare part & aneka peralatan kapal yang anda inginkan</h5>
                        <div class="py-2"></div>
                        <div class="clear"></div>
                        <!-- start form -->
                        <form class="form-inline" method="get" action="<?php echo CHtml::normalizeUrl(array('/product/index')); ?>">
                          <input type="text" name="q" class="form-control mb-2 mr-sm-2" placeholder="Ketik pencarian anda">
                          <select name="category" id="" class="form-control mb-2 mr-sm-2">
                              <option value="">Pilih Category</option>
                              <?php foreach ($CategoryAlls as $key => $value): ?>
                              <option value="<?php echo $value->id ?>"><?php echo $value->description->name ?></option>
                              <?php endforeach ?>
                          </select>

                          <button type="submit" class="btn btn-primary mb-2">Cari</button>
                        </form>
                        <!-- end form                   -->
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>

        <div class="clear py-1"></div>

        <div class="backs_block_widget-category px-5 pt-4">
            <div class="tops pb-2">
                <div class="breadcrumbs">
                    <nav aria-label="breadcrumb">
                      <ol class="breadcrumb mb-0 p-0">
                        <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>">Kategori PRODUK</a></li>
                        <?php if (isset($_GET['category-name'])): ?>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo str_replace('-', ' ', $_GET['category-name']) ?></li>
                        <?php endif ?>
                        <?php if (isset($_GET['type'])): ?>
                         <li class="breadcrumb-item active" aria-current="page"><?php echo str_replace('_', ' ', $_GET['type']) ?></li>   
                        <?php endif ?>
                      </ol>
                    </nav>
                </div>
                <div class="py-2"></div>
                <div class="clear"></div>
            </div>
            
            <div class="tops_prdn_pagelist pt-2 pb-5">
                <div class="row">
                    <div class="col-md-45">
                        <h4><?php if (isset($_GET['category-name'])): ?>
                            <?php echo strtoupper( str_replace('-', ' ', $_GET['category-name']) ) ?>
                        <?php else: ?>
                            <?php echo strtoupper( str_replace('_', ' ', $_GET['type']) ) ?>
                        <?php endif ?>
                        </h4>
                        <?php if (isset($_GET['q'])): ?>
                        Hasil Pencarian: <?php echo ucwords($_GET['q']). ', '; ?>
                          <?php if (isset($_GET['category']) && $_GET['category'] != ''): ?>
                          <?php
                          $criteria = new CDbCriteria;
                          $criteria->with = array('description');
                          $criteria->addCondition('t.parent_id = "0"');
                          $criteria->addCondition('t.type = :type');
                          $criteria->params[':type'] = 'category';
                          $criteria->addCondition('t.id = :ids');
                          $criteria->params[':ids'] = intval($_GET['category']);
                          $criteria->order = 'sort ASC';
                          $cat_search = PrdCategory::model()->find($criteria);
                          ?>
                          Category: <?php echo $cat_search->description->name; ?>
                          <?php endif ?>
                        <?php endif ?>
                    </div>
                    <div class="col-md-15">
                        <a href="<?php echo CHtml::normalizeUrl(array('/product/landing')); ?>" class="btn btn-link p-0 backs_def_p">Kembali Ke Kategori Produk &nbsp;&nbsp;<i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
                <!-- start list product -->
                <div class="lists_products_defitems">
                    <div class="row">
                        <?php foreach ($product->getData() as $key => $value): ?>
                        <div class="col-md-15 col-30">
                            <div class="items mb-3">
                                <div class="pict">
                                    <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $value->id, 'product-name'=>Slug::Create($value->description->name) )); ?>">
                                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(260,260, '/images/product/'. $value->image , array('method' => 'adaptiveResize', 'quality' => '90')); ?>" alt="<?php echo $value->description->name ?>" class="img-fluid">
                                    </a>
                                </div>
                                <div class="infos py-2">
                                    <a href="<?php echo CHtml::normalizeUrl(array('/product/posts', 'id'=> $value->id, 'product-name'=>Slug::Create($value->description->name) )); ?>"><h5><?php echo $value->description->name ?></h5></a>
                                    <p>
                                        <b>Rp <?php echo ($value->harga) ?></b> 
                                        <?php if ($value->harga_coret != 0 && $value->harga_coret !== null): ?>
                                        &nbsp;<small>Rp 15,000,000,-</small>
                                        <?php endif ?>
                                    </p>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <?php endforeach ?>
                    </div>
                </div>
                <!-- end list product -->
                <div class="py-2"></div>
                <div class="block-pagination text-center">
                    <?php 
                       $this->widget('CLinkPager', array(
                          'pages' => $product->getPagination(),
                          'header'=>'',
                          'footer'=>'',
                          'lastPageCssClass' => 'd-none',
                          'firstPageCssClass' => 'd-none',
                          'nextPageCssClass' => 'd-none',
                          'previousPageCssClass' => 'd-none',
                          'itemCount'=> $product->totalItemCount,
                          'htmlOptions'=>array('class'=>'pagination text-center justify-content-center pagination-sm'),
                          'selectedPageCssClass'=>'active',
                      ));
                   ?>
               </div>

            <div class="clearfix"></div>
        </div>
        <div class="py-5"></div>
        <div class="clear clearfix"></div>
        </div>
    </section>

    <div class="clear"></div>
</section>

<script type="text/javascript">
    $(function(){
        $('.outer_block_footer_top').addClass('backs-white');

        $('.block-pagination ul li').addClass('page-item');
        $('.block-pagination ul li a').addClass('page-link');
    })
</script>