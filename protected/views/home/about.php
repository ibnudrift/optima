
<section class="cover-insides prelatife">
  <div class="prelatife container">
    <div class="inners">
      <div class="row">
        <div class="col-md-20 my-auto order-sm-1 order-2">
          <div class="boxed-text">
            <h1 class="mb-0">Profil</h1>
            <h6 class="mb-0"><?php echo $this->setting['about_hero_title'] ?></h6>
            <p><i><?php echo $this->setting['about_hero_subtitle'] ?></i></p>
          </div>
        </div>
        <div class="col-md-40 order-sm-2 order-1">
          <div class="pictures">
            <img src="<?php echo $this->assetBaseurl. '../../images/static/'. $this->setting['about_hero_image']; ?>" alt="" class="img img-fluid">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="about_outer_content back-white">
  <div class="prelatife container">
    <div class="py-4"></div>
    <div class="py-3 d-none d-sm-block"></div>
    
    <div class="content-text text-center insubpage">
      <h2 class="title-page"><?php echo $this->setting['about_ntop_title'] ?></h2>
      <?php echo $this->setting['about1_content'] ?>
      
      <div class="py-2"></div>
      <p><img src="<?php echo $this->assetBaseurl.'../../images/static/'. $this->setting['about1_banner'] ?>" alt="" class="img img-fluid w-100"></p>
      <div class="py-2 my-1"></div>

      <?php echo $this->setting['about2_content']; ?>

      <div class="clear"></div>
    </div>

    <div class="py-3"></div>
  </div>
</section>

<script type="text/javascript">
    $(function(){
        $('.outer_block_footer_top').addClass('backs-white');
    })
</script>