
<section class="cover-insides prelatife">
  <div class="prelatife container">
    <div class="inners">
      <div class="row">
        <div class="col-md-15 my-auto order-sm-1 order-2">
          <div class="boxed-text">
            <h1 class="mb-0">Hubungi Kami</h1>
            <h6 class="mb-0"><?php echo $this->setting['contact_hero_title'] ?></h6>
            <p><i><?php echo $this->setting['contact_hero_subtitle'] ?></i></p>
          </div>
        </div>
        <div class="col-md-45 order-sm-2 order-1">
          <div class="pictures">
            <img src="<?php echo $this->assetBaseurl. '../../images/static/'. $this->setting['contact_hero_image']; ?>" alt="" class="img img-fluid">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="about_outer_content back-white">
  <div class="prelatife container">
    <div class="py-4"></div>
    <div class="py-3 d-none d-sm-block"></div>
    
    <div class="content-text text-center insubpage">
      <h2 class="title-page">HUBUNGI OPTIMA MARINE PARTS SHOP</h2>
      <h5><?php echo $this->setting['contact_content_top'] ?></h5>
      
      <div class="py-2"></div>
      <div class="contacts_info">
        <p><strong>PT Optima Prima Metal Sinergi </strong><br>
          <?php echo nl2br($this->setting['contact_address']); ?>
          <strong><br><a target="_blank" href="<?php echo $this->setting['contact_map_address'] ?>" class="view_map">View on Google Map</a></strong>
        </p>
        <p><strong>Telephone</strong><br><?php echo $this->setting['contact_phone'] ?></p>
        <p><strong>Email</strong><br><a href="mailto:info@opms.co.id">info@opms.co.id</a></p>
        <p><strong>Whatsapp</strong><br><a href="https://api.whatsapp.com/send?phone=<?php echo str_replace(' ', '', $this->setting['contact_wa']); ?>"><?php echo '+'.$this->setting['contact_wa'] ?> (Click to chat)</a></p>
        <div class="clear"></div>
      </div>

      <div class="clear"></div>
    </div>

    <div class="py-4"></div>
  </div>
</section>

<script type="text/javascript">
    $(function(){
        $('.outer_block_footer_top').addClass('backs-white');
    })
</script>